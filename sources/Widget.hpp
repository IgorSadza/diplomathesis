/*
    Widget.hpp
    Purpose: Widget class

    @author Igor Sadza
    @version 0.1 - 10/03/19
*/
#ifndef WIDGET_HPP
#define WIDGET_HPP

#include "CommonIncludes.hpp"
#include "Object.hpp"

class Widget {
    private:
        Object *m_widgetObject;
        GLboolean m_blockInput;
    public:
        Widget();
        virtual GLvoid widgetLogic() = 0;
        virtual GLvoid widgetRender() = 0;
        Object &getWidgetObject();
        GLboolean &blockInput();
};

#endif //WIDGET_HPP