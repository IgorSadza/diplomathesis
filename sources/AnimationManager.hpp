/*
    AnimationManager.hpp
    Purpose: State playground for testing.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef ANIMATION_MANAGER_HPP
#define ANIMATION_MANAGER_HPP

#include "CommonIncludes.hpp"
#include "Object.hpp"
#include "EffectManager.hpp"

class AnimationManager {
    private:
        Object              m_object;
        std::vector<Image*> m_frames;
        GLuint              m_framesCounter;
        GLfloat             m_frameDuration;
        GLfloat             m_currentDuration;
        GLboolean           m_isAnimationEnd;
        EffectManager       *m_effectManager;
        
        GLvoid loadFrames(const std::string &t_pathToFirstAnimationFile);
    public:
        AnimationManager(const std::string &t_pathToFirstAnimationFile);
        Object &getAnimationManagerObject();
        GLvoid animationLogic();
        GLvoid animationRender();
        GLboolean isAnimationFinish();
        EffectManager &getEffectManager();
};
#endif //ANIMATION_MANAGER_HPP