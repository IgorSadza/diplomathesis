/*
    CursorManager.hpp
    Purpose: Cursor manager.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#ifndef CURSOR_MANAGER_HPP
#define CURSOR_MANAGER_HPP

#include "CursorManager.hpp"
#include "Object.hpp"
#include "ObjectParticleGenerator.hpp"
#include "Primitive.hpp"

class CursorManager {
    private:
        static Primitive m_cursorPrimitive;
        static ObjectParticleGenerator *m_cursorParticleGenerator;
        CursorManager();
    public:
        static CursorManager &initCursorManager();
        static Primitive &getCursorPrimitive();
        static GLvoid renderCursorEffect();
};

#endif //CURSOR_MANAGER_HPP