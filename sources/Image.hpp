/*
    Image.hpp
    Purpose: Load image from file.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef IMAGE_HPP
#define IMAGE_HPP

#include "CommonIncludes.hpp"

class Image {
    private:
        std::string m_name;
        glm::ivec2  m_size;
        GLubyte     *m_pixels;
        GLuint      m_textureID;
    public:
        Image(const std::string &t_pathToImage);
        Image();
        std::string getName();
        glm::ivec2 &getSize();        
        GLvoid bindImage();
        GLubyte *getImagePixels();
};

#endif //IMAGE_HPP