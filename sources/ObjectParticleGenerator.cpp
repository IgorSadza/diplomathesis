/*
    ObjectParticleGenerator.hpp
    Purpose: Particle generator for objects.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#include "ObjectParticleGenerator.hpp"
#include "SharedResources.hpp"
#include "PrimitiveRenderer.hpp"

ObjectParticleGenerator::ObjectParticleGenerator(const std::string &t_pathShaderFile, Primitive *t_primitive, GLuint t_particleAmount, glm::vec2 t_particleSize, glm::vec2 t_particlePosition, glm::vec3 t_particleColor, const std::string &t_pathImageFile)
    : m_primitive           (t_primitive)    
    , m_particleSize        (t_particleSize)
    , m_partcilePosition    (t_particlePosition)
    , m_particleColor       (t_particleColor)
    , m_isImage             (false)
    , m_isParticleBlocked   (false) {

    m_shader = Shader(t_pathShaderFile); 

    if (t_pathImageFile != std::string()) {
        m_image = Image(t_pathImageFile);
        m_isImage = true;     
    }


    for (int i = 0; i < t_particleAmount; i++) {
        Particle particle;
        m_particles.push_back(particle);
    }
    randomiseParticles();
}

GLvoid ObjectParticleGenerator::randomiseParticles() {
    for (auto& ite: m_particles) {
        if (ite.getLife() < 0.0f) {
            glm::vec4 randColor;
            glm::vec2 randSize;
            GLfloat randVelocity;

            // Randomise color of particle
            randColor = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
            for (int i = 0; i < 3; i++) {
                if (m_particleColor[i] > 0.0f) {
                    randColor[i] = ((rand() % 100) / 100.0f);                
                }
            }

            // Randomise velocity of particle
            randVelocity = 0.1 + ((rand() % 100) / 100.0f); 

            // Send all rand vars to particle
            ite.setVelocity(randVelocity);
            ite.setPosition(m_partcilePosition + m_primitive->getPosition());
            ite.setSize(m_particleSize);
            ite.setColor(randColor);        
            ite.setLife(1.0f);     
        }
    }
}

GLvoid ObjectParticleGenerator::updateParticles() {
    for (auto& ite: m_particles) {
        ite.getPosition().y -= 0.25 - ite.getVelocity() + SharedResources::deltaTime;     
        ite.getColor().a -= 0.5 * ite.getVelocity() * SharedResources::deltaTime;
        ite.getLife() -= ite.getVelocity() + SharedResources::deltaTime;
    }
}


GLvoid ObjectParticleGenerator::renderParticles() {

updateParticles();
randomiseParticles();

m_shader.useShader();
if(m_isImage) {
m_image.bindImage();    
}
    


    glBlendFunc(GL_SRC_ALPHA, GL_ONE);    
if(!m_isParticleBlocked) {
    for (auto& ite: m_particles) {
    glm::mat4 modelMatrix;
    modelMatrix = glm::translate(modelMatrix, glm::vec3(ite.getPosition(), 0.0f));
    modelMatrix = glm::scale(modelMatrix, glm::vec3(ite.getSize(), 0.0f));

    m_shader.sendVariable("t_model", modelMatrix);
    m_shader.sendVariable("t_color", ite.getColor());
    PrimitiveRenderer::renderPrimitive();      
    }
}
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

GLboolean &ObjectParticleGenerator::isParticleBlockes() {
    return m_isParticleBlocked;
}