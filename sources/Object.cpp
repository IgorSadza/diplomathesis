/*
    StatePlayground.cpp
    Purpose: State playground for testing.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "Object.hpp"
#include "PrimitiveRenderer.hpp"
#include "TextRenderer.hpp"
#include "ResourceManager.hpp"
#include "TextFileParser.hpp"

Object::Object() {
}

GLvoid Object::setImage(const std::string &t_pathToImageFile) {
  m_image = Image(t_pathToImageFile);
  m_primitive.getSize() = m_image.getSize();
}

GLvoid Object::setShader(const std::string &t_pathToShaderFile) {
  std::string tmpName = TextFileParser::getParsedName(t_pathToShaderFile);
  if (!ResourceManager<Shader>::searchObject(tmpName)) {
    m_shader = Shader(t_pathToShaderFile);    
  } else {
    m_shader = ResourceManager<Shader>::getObject(tmpName);
  }

}

GLvoid Object::setText(const std::string &t_textToRender, 
                       const std::string &t_pathToFontFile, 
                       GLuint t_textSize) {

 m_text = Text(t_textToRender, t_pathToFontFile, &m_primitive, t_textSize);
}

Image &Object::getImage()         { return m_image; }

Shader &Object::getShader()       { return m_shader; }

Primitive &Object::getPrimitive() { return m_primitive; }

Text &Object::getText()           { return m_text; }

GLvoid Object::renderObject() {
    
  m_shader.useShader();
  m_image.bindImage();

  glm::mat4 modelMatrix;
  modelMatrix = glm::translate(modelMatrix, glm::vec3(m_primitive.getPosition(), 0.0f));
  modelMatrix = glm::scale(modelMatrix, glm::vec3(m_primitive.getSize(), 0.0f));

  m_shader.sendVariable("t_model", modelMatrix);
  m_shader.sendVariable("t_color", m_primitive.getColor());

  PrimitiveRenderer::renderPrimitive();
  TextRenderer::renderText(&m_text);
}

GLvoid Object::restoreObject() {
  m_image = m_savedObjectState->m_image;
  m_primitive = m_savedObjectState->m_primitive;
  m_shader = m_savedObjectState->m_shader;
  m_text = m_savedObjectState->m_text;
}

GLvoid Object::saveObject() {
  m_savedObjectState = new Object();
  m_savedObjectState->m_image = this->m_image;
  m_savedObjectState->m_primitive = this->m_primitive;
  m_savedObjectState->m_shader = this->m_shader;
  m_savedObjectState->m_text = this->m_text;
}