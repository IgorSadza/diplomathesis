/*
    Text.cpp
    Purpose: Text struct for render purpose.

    @author Igor Sadza
    @version 0.1 - 10/03/19
*/
#include "Text.hpp"
#include "ResourceManager.hpp"
#include "TextFileParser.hpp"

Text::Text() {}

Text::Text(const std::string    &t_textToRender,
           const std::string    &t_pathToFontFile, 
           Primitive            *t_primitive,
           GLuint                t_fontSize)
           : m_textToRender     (t_textToRender)
           , m_givenPrimitive   (t_primitive) {

  std::string fontName = TextFileParser::getParsedName(t_pathToFontFile);

  if (!ResourceManager<FontManager>::searchObject(fontName)) {
    m_font = FontManager(t_pathToFontFile, t_fontSize);
  } else {
    m_font = ResourceManager<FontManager>::getObject(fontName);
  }

  if (m_givenPrimitive != nullptr) {
      centeringTextToGivenPrimitive();
  } else {
      m_givenPrimitive = new Primitive();
  }
}

std::string Text::getTextToRender() { return m_textToRender; }

FontManager Text::getFont()         { return m_font; }

glm::vec2 &Text::getTextPosition() {
  return m_textPosition;
}

GLvoid Text::centeringTextToGivenPrimitive() {

  float lineLenght;
  std::string::const_iterator ite;
  for (ite = m_textToRender.begin(); ite != m_textToRender.end(); ite++) {
    Glyph glyph = *m_font.getFont()[*ite];
    lineLenght += (glyph.getGlyphAdvance() >> 6);
  }

  glm::vec2 textRectangle;
  textRectangle.x = lineLenght;
  textRectangle.y = m_font.getFont()['H']->getGlyphSize().y;

  m_textPosition.x = m_givenPrimitive->getPosition().x + m_givenPrimitive->getSize().x / 2 - textRectangle.x / 2;
  m_textPosition.y = m_givenPrimitive->getPosition().y  + m_givenPrimitive->getSize().y / 2 - textRectangle.y / 2;

  m_textPosition + m_givenPrimitive->getPosition();
}