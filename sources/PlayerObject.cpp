/*
    PlayerObject.cpp
    Purpose: Player object.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "PlayerObject.hpp"
#include "InputManager.hpp"
#include "SharedResources.hpp"

PlayerObject::PlayerObject()
    : m_playerVelocity(5.0f)
    , m_shotObject(NULL) { 
    m_playerObject.setImage("resources/sprites/player/PNG/PlayerSprite.png");
    m_playerObject.setShader("resources/shaders/rectangle.shader");
    m_partcileGenerator = new ObjectParticleGenerator("resources/shaders/circle.shader", &m_playerObject.getPrimitive(), 5, glm::vec2(7.5f), glm::vec2(46.25f, 100), glm::vec3(1.0, 1.0, 0.0f));
}

GLfloat waitTime = 0.0f;
GLvoid PlayerObject::movePlayer() {

    m_partcileGenerator->isParticleBlockes() = true;

    if (glfwGetKey(SharedResources::window, GLFW_KEY_UP) == GLFW_PRESS) {
        m_partcileGenerator->isParticleBlockes() = false;     
        m_playerObject.getPrimitive().getPosition().y -= m_playerVelocity;

    } else if (glfwGetKey(SharedResources::window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        m_playerObject.getPrimitive().getPosition().y += m_playerVelocity;
    }

    if(glfwGetKey(SharedResources::window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        m_playerObject.getPrimitive().getPosition().x += m_playerVelocity;
    } else if(glfwGetKey(SharedResources::window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        m_playerObject.getPrimitive().getPosition().x -= m_playerVelocity;
    }
waitTime += SharedResources::deltaTime;
    if(waitTime > 0.2f) {
        if (glfwGetKey(SharedResources::window, GLFW_KEY_SPACE) == GLFW_PRESS) {
            ShotObject shotObject(&m_playerObject.getPrimitive());
            m_shots.push_back(shotObject);
            InputManager::getKeyboardKeys()[GLFW_KEY_SPACE] = false;
        }
        waitTime = 0.0f;
    }

}

GLvoid PlayerObject::playerLogic() {
    for (auto& ite: m_shots) {
        ite.shotLogic();
        ite.shotRender();
    }
    checkShots();
    movePlayer();
}   

GLvoid PlayerObject::playerRender() {

    m_partcileGenerator->renderParticles();        
    m_playerObject.renderObject();
}

GLvoid PlayerObject::checkShots() {
    for (int i = 0; i < m_shots.size(); i++) {
        if (m_shots[i].getShotPrimitive().getPosition().y < 0.0f) {
            m_shots.erase(m_shots.begin() + i);
        }
    }
}