/*
    WidgetButton.cpp
    Purpose: Widget button

    @author Igor Sadza
    @version 0.1 - 10/03/19
*/
#include "WidgetButton.hpp"
#include "TextFileParser.hpp"
#include "CursorManager.hpp"
#include "InputManager.hpp"

WidgetButton::WidgetButton(const std::string &t_sourceForInitialization) : Widget(){
  for (auto &ite :TextFileParser::getParasedVariables(t_sourceForInitialization)) {
    if (ite.m_key == "Position") {
        getWidgetObject().getPrimitive().setPosition(TextFileParser::parseVariable<glm::vec2>(ite.m_variable));
    } else if (ite.m_key == "Color") {
        getWidgetObject().getPrimitive().setColor(TextFileParser::parseVariable<glm::vec4>(ite.m_variable));
    } else if (ite.m_key == "Image") {
        getWidgetObject().setImage(ite.m_variable);
    } else if (ite.m_key == "Shader") {
        getWidgetObject().setShader(ite.m_variable);
    } else if (ite.m_key == "Text") {
        getWidgetObject().setText(ite.m_variable);
    }
  }
  getWidgetObject().saveObject();
  blockInput() = true;
}

GLvoid WidgetButton::checkClick() {

  if (checkPrimitiveCollision(CursorManager::getCursorPrimitive(), getWidgetObject().getPrimitive())) {

   getWidgetObject().getPrimitive().setColor(0.85f, 0.85f, 0.85f, 1.0f);
    if (InputManager::getMouseKeys()[GLFW_MOUSE_BUTTON_1] && !blockInput()) {
      getWidgetObject().getText().getTextPosition().y += 0.7f;
      blockInput() = true;
    }

  } else {
    getWidgetObject().restoreObject();
    blockInput() = false;
  }
}

GLvoid WidgetButton::widgetLogic() {
    checkClick();
}

GLvoid WidgetButton::widgetRender() {
   getWidgetObject().renderObject();
}