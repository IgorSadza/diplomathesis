/*
    CommonIncludes.hpp
    Purpose: File for global settings.

    @author Igor Sadza
    @version 0.1 - 06/03/19
*/

#include "Settings.hpp"

namespace Settings {
const GLchar *name      = "DiplomaThesis";
const GLfloat width     = 800.0f;
const GLfloat height    = 600.0f;
}
