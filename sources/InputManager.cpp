/*
    InputManager.cpp
    Purpose: Input manager.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#include "InputManager.hpp"

GLboolean *InputManager::m_mouseKeys;
GLboolean *InputManager::m_keyboardKeys;

InputManager::InputManager() {
    m_mouseKeys = new GLboolean[5];
    m_keyboardKeys = new GLboolean[5];
}

InputManager &InputManager::initInputManager() {
    static InputManager inputManager;
    return inputManager;
}

GLboolean *InputManager::getMouseKeys() {
    return m_mouseKeys;        
}

GLboolean *InputManager::getKeyboardKeys() {
    return m_keyboardKeys;
}