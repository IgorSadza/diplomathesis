/*
    EffectManager.cpp
    Purpose: Effect like dash etc. 

    @author Igor Sadza
    @version 0.1 - 11/03/19
*/
#include "EffectManager.hpp"
#include "SharedResources.hpp"

EffectManager::EffectManager(Primitive *t_primitive)
    : m_primitive   (t_primitive)
    , m_stopEffect  (false)
    , m_reverse     (false)
    , m_isEffectEnd (false) {
    m_primitive->getColor().w = 0.0f;
}

GLvoid EffectManager::runEffectDash(GLboolean t_reverse) {
    if(!stopEffect()) {
        if(!t_reverse) {
            if (m_primitive->getColor().w >= 0.0f) {
                m_primitive->getColor().w -= SharedResources::deltaTime;
            }else {
                m_isEffectEnd = true;
            }
        } else {
            if (m_primitive->getColor().w <= 1.0f) {
                m_primitive->getColor().w += SharedResources::deltaTime;
            }else {
                m_isEffectEnd = true;
            }         
        }
    }
}

GLboolean &EffectManager::stopEffect() {
    return m_stopEffect;
}

GLboolean &EffectManager::isEffectEnd() {
    return m_isEffectEnd;
}