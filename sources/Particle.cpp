/*
    Particle.cpp
    Purpose: Particle.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/

#include "Particle.hpp"

Particle::Particle()
    : Primitive()
    , m_velocity(1.0f)
    , m_life(0.0f) {
}

GLvoid Particle::setLife(GLfloat t_life) {
    m_life = t_life;
}

GLvoid Particle::setVelocity(GLfloat t_velocity) {
    m_velocity = t_velocity;
}

GLfloat &Particle::getLife() {
    return m_life;
}

GLfloat &Particle::getVelocity() {
    return m_velocity;
}