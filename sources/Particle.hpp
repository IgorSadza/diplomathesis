/*
    Particle.hpp
    Purpose: Particle.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#ifndef PARTICLE_HPP
#define PARTICLE_HPP

#include "CommonIncludes.hpp"
#include "Primitive.hpp"

class Particle : public Primitive {
private:
  GLfloat m_velocity;
  GLfloat m_life;

public:
  Particle();

  GLfloat &getLife();
  GLfloat &getVelocity();

  GLvoid setLife(GLfloat t_life);
  GLvoid setVelocity(GLfloat t_velocity);
};

#endif //PARTICLE_HPP