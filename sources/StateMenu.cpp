#include "StateMenu.hpp"
#include "StatePlayground.hpp"

static StateMenu *stateMenu;

StateMenu::StateMenu(const std::string &t_pathToStateSource) {
  stateMenu = this;
  State::Register("StateMenu", stateMenu);
  State::setInstance("StateMenu");
}

GLvoid StateMenu::stateLogic() {
}

GLvoid StateMenu::stateRender() {}