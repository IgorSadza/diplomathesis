/*
    Object.hpp
    Purpose: Create object thath handle shader, fonts, image, rectangle 
             and send this variables to specyfic renderers. 

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef OBJECT_HPP
#define OBJECT_HPP

#include "CommonIncludes.hpp"
#include "Shader.hpp"
#include "Image.hpp"
#include "Primitive.hpp"
#include "Text.hpp"

class Object {
    private:
        Image       m_image;
        Shader      m_shader;
        Text        m_text;
        Primitive   m_primitive;        

        Object      *m_savedObjectState;        
    public:
        Object();

        GLvoid setImage     (const std::string &t_pathToImageFile);
        GLvoid setShader    (const std::string &t_pathToShaderFile);
        GLvoid setText      (const std::string &t_textToRender, 
                             const std::string &t_pathToFontFile = "resources/fonts/04b_30.ttf", 
                             GLuint t_textSize = 15);

        Image       &getImage();
        Shader      &getShader();        
        Primitive   &getPrimitive();
        Text        &getText();

        GLvoid saveObject();
        GLvoid restoreObject();
        GLvoid renderObject();
};

static GLboolean checkPrimitiveCollision(Primitive t_one, Primitive t_two) {

    bool collisionX = t_one.getPosition().x + t_one.getSize().x >= t_two.getPosition().x &&
                      t_two.getPosition().x + t_two.getSize().x >= t_one.getPosition().x;

    bool collisionY = t_one.getPosition().y + t_one.getSize().y >= t_two.getPosition().y &&
                      t_two.getPosition().y + t_two.getSize().y >= t_one.getPosition().y;

    return collisionX && collisionY;
}

#endif // OBJECT_HPP