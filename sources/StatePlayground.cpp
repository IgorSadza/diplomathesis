/*
    StatePlayground.cpp
    Purpose: State playground for testing.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "StatePlayground.hpp"
#include "Primitive.hpp"
#include "TextFileParser.hpp"
#include "Image.hpp"
#include "Shader.hpp"
#include "Object.hpp"
#include "PrimitiveRenderer.hpp"
#include "SceneParticleGenerator.hpp"
#include "CursorManager.hpp"
#include "FontManager.hpp"
#include "Text.hpp"
#include "TextRenderer.hpp"
#include "InputManager.hpp"
#include "WidgetButton.hpp"
#include "AnimationManager.hpp"
#include "PlayerObject.hpp"
#include "SharedResources.hpp"

static StatePlayground *statePlayground;

Object                      *obj_0;
//Text                        *txt;
SceneParticleGenerator      *par;

std::vector<WidgetButton> widgets;

AnimationManager *animationManager;

PlayerObject *playerObject;

StatePlayground::StatePlayground() {
  statePlayground = this;
  State::Register("StatePlayground", statePlayground);
  State::setInstance("StatePlayground");


  // * TextFileParser works.
  // TextFileParser tfp("resources/states/play.state");
  // for (auto& ite: tfp.getSections()) {
  //   for (auto& it: tfp.getParasedVariables(ite.m_source)) {
  //     std::cout << it.m_key << " " << it.m_variable << std::endl;
  //   }
  // }

  // * Image works.
  // Image *img = new Image("resources/sprites/widgets/widget_menu.png");
  // img->bindImage();
  // img->getName();
  // std::cout << img->getSize().x << " " << img->getSize().y << std::endl; 

  // * Shader works.
  //Shader *sha = new Shader("resources/shaders/rectangle.shader");
  //sha->getName();
  //sha->useShader();

  // * Object works.
  // obj_0 = new Object();
  // obj_0->setImage("resources/sprites/widgets/widget_menu.png");
  // obj_0->setShader("resources/shaders/rectangle.shader");
  // obj_0->getPrimitive().setPosition(300.0f);  
  // obj_0->setText("Igor");
  // obj_0->saveObject();

  // * ObjectRenderer works.
  PrimitiveRenderer::initRenderer();

  // * ParticleGenerator works.
  par = new SceneParticleGenerator("resources/shaders/particle.shader", 500, glm::vec2(10.0f), glm::vec2(800, -300), glm::vec2(800, 600), glm::vec3(1.0f, 0.0f, 0.0f));

  // * FontManager works.
  //FontManager *fm = new FontManager("resources/fonts/04b_30.ttf", 15);
  //fm->getFont();

  // * Text works.
  //txt = new Text("Igor", "resources/fonts/04b_30.ttf", &obj.getPrimitive());

  // * TextRenderer works.
  TextRenderer::initTextRenderer();

  // * WidgetButton works.
  TextFileParser tfp("resources/states/menu.state");
  for (auto& ite: tfp.getSections()) {
    if (ite.m_header == "WidgetButton") {
      WidgetButton wb(ite.m_source);
      widgets.push_back(wb);      
    }
  }

  // * AnimationManager works.
  animationManager = new AnimationManager("resources/sprites/animations/logo/0.png");

  // * PlayerObject works
  playerObject = new PlayerObject();
  //glfwSetInputMode(SharedResources::window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
}
GLvoid StatePlayground::stateLogic() {
  // if (checkPrimitiveCollision(CursorManager::getCursorPrimitive(), obj_0->getPrimitive())) {

    // obj_0->getPrimitive().setColor(0.85f);
    // if (InputManager::getMouseKeys()[GLFW_MOUSE_BUTTON_1] && !InputManager::blockInput()) {
    //   obj_0->getText().getTextPosition().y += 0.7f;
    //   InputManager::blockInput() = true;
    // }

  // } else {
  //   obj_0->restoreObject();
  //   InputManager::blockInput() = false;
  // }

  for (auto &ite : widgets) {
    ite.widgetLogic();
  }

  // (!animationManager->isAnimationFinish())
  // ? animationManager->getEffectManager().runEffectDash(true)
  // : animationManager->getEffectManager().runEffectDash();
  // animationManager->animationLogic();

  playerObject->playerLogic();
}

GLvoid StatePlayground::stateRender() {
  //animationManager->animationRender();
  par->renderParticles();
  //TextRenderer::renderText(txt);
  //obj_0->renderObject();

  for (auto &ite : widgets) {
   ite.widgetRender();
  };
  CursorManager::renderCursorEffect();

  playerObject->playerRender();
}