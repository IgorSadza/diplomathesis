/*
    ShotObject.hpp
    Purpose: Player shot object.

    @author Igor Sadza
    @version 0.1 - 11/03/19
*/
#include "ShotObject.hpp"
#include "PrimitiveRenderer.hpp"

ShotObject::ShotObject() {
    
}

ShotObject::ShotObject(Primitive *t_primitive) {
    m_shotObject.setShader("resources/shaders/circle.shader");
    glm::vec2 position;
    position.x = t_primitive->getPosition().x + t_primitive->getSize().x / 2 - 5.0f;
    position.y = t_primitive->getPosition().y;
    m_shotObject.getPrimitive().setPosition(position);
    m_shotObject.getPrimitive().setSize(10.0f);
}

GLvoid ShotObject::shotLogic() {
    m_shotObject.getPrimitive().getPosition().y -= 10.0f;
}

GLvoid ShotObject::shotRender() {
    m_shotObject.renderObject();
}

Primitive ShotObject::getShotPrimitive() {
    return m_shotObject.getPrimitive();
}