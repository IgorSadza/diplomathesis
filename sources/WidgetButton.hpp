/*
    WidgetButton.hpp
    Purpose: Widget button

    @author Igor Sadza
    @version 0.1 - 10/03/19
*/
#ifndef WIDGET_BUTTON_HPP
#define WIDGET_BUTTON_HPP

#include "CommonIncludes.hpp"
#include "Widget.hpp"

class WidgetButton : public Widget {
    private:
        GLvoid checkClick();
    public:
        WidgetButton(const std::string &t_sourceForInitialization);
        GLvoid widgetLogic();
        GLvoid widgetRender();
};

#endif //WIDGET_BUTTON_HPP