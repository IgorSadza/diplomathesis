/*
    InputManager.hpp
    Purpose: Input manager.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#ifndef INPUT_MANAGER_HPP
#define INPUT_MANAGER_HPP

#include "CommonIncludes.hpp"
#include "SharedResources.hpp"
#include "Object.hpp"

class InputManager {
    private:
        InputManager();
        static GLboolean *m_mouseKeys;
        static GLboolean *m_keyboardKeys;
    public:
        static InputManager &initInputManager();
        static GLboolean *getMouseKeys();
        static GLboolean *getKeyboardKeys();
};

#endif // INPUT_MANAGER_HPP
