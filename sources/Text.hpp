/*
    Text.hpp
    Purpose: Text object for render purpose.

    @author Igor Sadza
    @version 0.1 - 10/03/19
*/
#ifndef TEXT_HPP
#define TEXT_HPP

#include "CommonIncludes.hpp"
#include "FontManager.hpp"
#include "Primitive.hpp"

class Text {
private:
  std::string m_textToRender;
  FontManager m_font;
  Primitive   *m_givenPrimitive;
  glm::vec2   m_textPosition;
  glm::vec2   m_finalPosition;
  GLvoid centeringTextToGivenPrimitive();
public:
  Text();
  Text(const std::string &t_textToRender,
       const std::string &t_pathToFontFile,
       Primitive *t_primitive = nullptr,    
       GLuint t_fontSize = 15);

  std::string getTextToRender();
  FontManager getFont();
  glm::vec2 &getTextPosition();
};

#endif // TEXT_HPP