/*
    SceneParticleGenerator.hpp
    Purpose: Particle generator for background scene.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#ifndef SCENE_PARTICLE_GENERATOR_HPP
#define SCENE_PARTICLE_GENERATOR_HPP

#include "CommonIncludes.hpp"
#include "Particle.hpp"
#include "Shader.hpp"

class SceneParticleGenerator {
    private:
        Shader *m_shader;
        glm::vec2 m_rangeFrom;
        glm::vec2 m_rangeTo;
        glm::vec3 m_rangeColor;
        glm::vec2 m_particleSize;

        std::vector<Particle> m_particles;

        GLvoid updateParticles();
        GLvoid randomiseParticles();
    public:
        SceneParticleGenerator(const std::string &t_pathToSceneParticleGeneratorShader, GLuint t_particleAmount, glm::vec2 t_particleSize, glm::vec2 t_rangeFrom, glm::vec2 t_rangeTo, glm::vec3 t_rangeColor);
        GLvoid renderParticles();
};

#endif //SCENE_PARTICLE_GENERATOR_HPP