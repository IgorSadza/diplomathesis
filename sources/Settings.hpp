/*
    CommonIncludes.hpp
    Purpose: File for global settings.

    @author Igor Sadza
    @version 0.1 - 06/03/19
*/
#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include "CommonIncludes.hpp"

namespace Settings {
extern const GLchar *name;
extern const GLfloat width;
extern const GLfloat height;
}

#endif //SETTINGS_HPP
