/*
    Widget.hpp
    Purpose: Widget class

    @author Igor Sadza
    @version 0.1 - 10/03/19
*/
#include "Widget.hpp"

Widget::Widget() {
    m_widgetObject = new Object();
    m_blockInput = true;
}

Object &Widget::getWidgetObject() {
    return *m_widgetObject;
}

GLboolean &Widget::blockInput() {
    return m_blockInput;
}