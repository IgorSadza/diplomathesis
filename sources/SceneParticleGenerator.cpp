/*
    SceneParticleGenerator.hpp
    Purpose: Particle generator for background scene.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#include "SceneParticleGenerator.hpp"
#include "SharedResources.hpp"
#include "PrimitiveRenderer.hpp"

SceneParticleGenerator::SceneParticleGenerator(const std::string &t_pathShaderFile, GLuint t_particleAmount, glm::vec2 t_particleSize, glm::vec2 t_rangeFrom, glm::vec2 t_rangeTo, glm::vec3 t_rangeColor)
    : m_rangeFrom(t_rangeFrom)
    , m_rangeTo(t_rangeTo)
    , m_rangeColor(t_rangeColor)
    , m_particleSize(t_particleSize) {
    m_shader = new Shader(t_pathShaderFile);

    for (int i = 0; i < t_particleAmount; i++) {
        Particle particle;
        m_particles.push_back(particle);
    }
    randomiseParticles();
    randomiseParticles();
}

GLvoid SceneParticleGenerator::randomiseParticles() {
    for (auto& ite: m_particles) {
        if (ite.getLife() <= 0.0f || glm::all(glm::greaterThan(ite.getPosition(), m_rangeTo))) {
            glm::vec2 randStartingPosition;
            glm::vec4 randColor;
            glm::vec2 randSize;
            GLfloat randVelocity;

            // Randomise particle starting position
            for (int i = 0; i < 2; i++) {
                if (m_rangeFrom[i] > 0) {
                    randStartingPosition[i] = rand() % (GLint)m_rangeFrom[i];
                } else {
                    randStartingPosition[i] = -1.0f*(rand() % (GLuint)(-1.0f * m_rangeFrom[i]));
                }
            }

            // Randomise size of particle
            //for (int i = 0; i < 2; i++) {
                GLfloat scale = 0.1f + ((rand() % 100) / 100.0f);
            //}

            // Randomise color of particle
            randColor = glm::vec4(1.0f);
            for (int i = 0; i < 3; i++) {
                if (m_rangeColor[i] == 1.0f) {
                    randColor[i] = 0.5 + ((rand() % 100) / 100.0f);                
                } else {
                    randColor[i] = ((rand() % 30) / 100.0f);  
                }
            }

            // Randomise velocity of particle
            randVelocity = 0.1 + ((rand() % 100) / 100.0f); 

            // Send all rand vars to particle
            ite.setVelocity(randVelocity);
            ite.setPosition(randStartingPosition);
            ite.setSize(m_particleSize * scale);
            ite.setColor(randColor);        
            ite.setLife(1.0f);     
        }
    }
}

GLvoid SceneParticleGenerator::updateParticles() {
    for (auto& ite: m_particles) {
        ite.getPosition().y += 0.25 + ite.getVelocity() + SharedResources::deltaTime;
        ite.getColor().w -= 0.15 * ite.getVelocity() * SharedResources::deltaTime;
        ite.getLife() -= 0.15 * ite.getVelocity() * SharedResources::deltaTime;
    }
}


GLvoid SceneParticleGenerator::renderParticles() {

updateParticles();
randomiseParticles();

m_shader->useShader();

glBlendFunc(GL_SRC_ALPHA, GL_CONSTANT_COLOR);
for (auto& ite: m_particles) {
  glm::mat4 modelMatrix;
  modelMatrix = glm::translate(modelMatrix, glm::vec3(ite.getPosition(), 0.0f));
  modelMatrix = glm::scale(modelMatrix, glm::vec3(ite.getSize(), 0.0f));

  m_shader->sendVariable("t_model", modelMatrix);
  m_shader->sendVariable("t_color", ite.getColor());
  PrimitiveRenderer::renderPrimitive();      
}
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}