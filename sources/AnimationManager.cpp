/*
    AnimationManager.hpp
    Purpose: State playground for testing.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "AnimationManager.hpp"
#include "SharedResources.hpp"

AnimationManager::AnimationManager(const std::string &t_pathToFirstAnimationFile)
    : m_framesCounter(0)
    , m_frameDuration(0.1f)
    , m_currentDuration(0.0f)
    , m_isAnimationEnd(false) {
    loadFrames(t_pathToFirstAnimationFile);

    m_object.setImage(t_pathToFirstAnimationFile);
    m_object.getPrimitive().setSize(225.0f, 100.0f);
    m_object.getPrimitive().setPosition(287.5f, 225.0f);
    m_object.getPrimitive().setColor(1.0f, 1.0f, 1.0f, 1.0f);
    m_object.setShader("resources/shaders/rectangle.shader");

    m_effectManager = new EffectManager(&m_object.getPrimitive());
}

GLvoid AnimationManager::loadFrames(const std::string &t_pathToFirstAnimationFile) {
    
    std::string withoutExtension = t_pathToFirstAnimationFile; 
    size_t signPos = t_pathToFirstAnimationFile.find_last_of('.') - 1;

    withoutExtension.erase(withoutExtension.begin() + signPos, withoutExtension.end());

    std::string modifiedPath =  withoutExtension + std::to_string(0) + ".png";
    glm::ivec2 size;    
    GLubyte *pixels = stbi_load(modifiedPath.c_str(), &size.x, &size.y, nullptr, 0); 


    for (int i = 1; pixels; i++) {
        Image * img = new Image(modifiedPath);
        m_frames.push_back(img);              

        modifiedPath = withoutExtension + std::to_string(i+1) + ".png";
        pixels = stbi_load(modifiedPath.c_str(), &size.x, &size.y, nullptr, 0);
    }    
}

GLvoid AnimationManager::animationLogic() {

    // if (!m_effectManager->stopEffect()) {
    //     m_effectManager->runEffectDash();        
    // }


   //if (m_object.getPrimitive().getColor().w >= 1.0f) {
       //m_effectManager->stopEffect() = true;

        m_currentDuration += SharedResources::deltaTime;
        if(m_currentDuration >= m_frameDuration) {
            if (m_frames[m_framesCounter]) {
                m_object.getImage() = *m_frames[m_framesCounter++];
                m_currentDuration = 0.0f;
            }
            else {
                m_isAnimationEnd = true;
            }
        }        
    //}
    // else if (m_object.getPrimitive().getColor().w <= 0.0f) {
    //     m_effectManager->stopEffect() = true;    
    // }

}

GLvoid AnimationManager::animationRender() {
    m_object.renderObject();
}

GLboolean AnimationManager::isAnimationFinish() {
    return m_isAnimationEnd;
}

EffectManager &AnimationManager::getEffectManager() {
    return *m_effectManager;
}