/*
    EffectManager.cpp
    Purpose: Effect like dash etc. 

    @author Igor Sadza
    @version 0.1 - 11/03/19
*/
#ifndef EFFECT_MANAGER_HPP
#define EFFECT_MANAGER_HPP

#include "CommonIncludes.hpp"
#include "Primitive.hpp"

class EffectManager {
    private:
        Primitive *m_primitive;
        GLboolean m_stopEffect;
        GLboolean m_reverse;
        GLboolean m_isEffectEnd;
    public:
        EffectManager(Primitive *t_primitive);
        GLvoid runEffectDash(GLboolean t_reverse = false);
        GLboolean &stopEffect();
        GLboolean &isEffectEnd();
};

#endif //EFFECT_MANAGER_HPP