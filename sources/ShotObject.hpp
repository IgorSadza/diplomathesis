/*
    ShotObject.hpp
    Purpose: Player shot object.

    @author Igor Sadza
    @version 0.1 - 11/03/19
*/
#ifndef SHOT_OBJECT_HPP
#define SHOT_OBJECT_HPP

#include "CommonIncludes.hpp"
#include "Object.hpp"

class ShotObject {
    private:
        Object       m_shotObject;
        Primitive    m_primitive;
    public:
        ShotObject();
        ShotObject(Primitive *t_primitive);
        GLvoid shotLogic();
        GLvoid shotRender();
        Primitive getShotPrimitive();
};

#endif //SHOT_OBJECT_HPP