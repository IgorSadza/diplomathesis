/*
    CursorManager.cpp
    Purpose: Cursor manager.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#include "CursorManager.hpp"
#include "SharedResources.hpp"

Primitive CursorManager::m_cursorPrimitive;
ObjectParticleGenerator *CursorManager::m_cursorParticleGenerator;

CursorManager::CursorManager() {
    Image img("resources/sprites/cursors/cursor_normal.png");
    
    GLFWcursor *cursor;
    GLFWimage *image = new GLFWimage();

    image->pixels = img.getImagePixels();
    image->height = img.getSize().x;
    image->width = img.getSize().y;
    cursor = glfwCreateCursor(image, 0, 0);

    glfwSetCursor(SharedResources::window, cursor);

    m_cursorPrimitive.setPosition(0.0f);
    m_cursorPrimitive.setSize(10.0f);
    m_cursorPrimitive.setColor(1.0f);

    m_cursorParticleGenerator = new ObjectParticleGenerator("resources/shaders/rectangle.shader", &m_cursorPrimitive, 500, glm::vec2(25.0f), glm::vec2(0.0f), glm::vec3(0.5f, 0.7f, 0.0f), "resources/sprites/cursors/cursor_particleTest.png");
}

CursorManager &CursorManager::initCursorManager() {
    static CursorManager cursorManager;
    return cursorManager;
}

Primitive &CursorManager::getCursorPrimitive() {
    return m_cursorPrimitive;
}

GLvoid CursorManager::renderCursorEffect() {
    m_cursorParticleGenerator->renderParticles();
}