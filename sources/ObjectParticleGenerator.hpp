/*
    ObjectParticleGenerator.hpp
    Purpose: Particle generator for objects.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#ifndef OBJECT_PARTICLE_GENERATOR_HPP
#define OBJECT_PARTICLE_GENERATOR_HPP

#include "CommonIncludes.hpp"
#include "Particle.hpp"
#include "Shader.hpp"
#include "Primitive.hpp"
#include "Image.hpp"

class ObjectParticleGenerator {
    private:
        Shader          m_shader;
        Primitive       *m_primitive;
        Image           m_image;

        glm::vec3 m_particleColor;
        glm::vec2 m_particleSize;
        glm::vec2 m_partcilePosition;
        GLboolean m_isImage;
        GLboolean m_isParticleBlocked;
        
        std::vector<Particle> m_particles;

        GLvoid updateParticles();
        GLvoid randomiseParticles();
    public:
        ObjectParticleGenerator(const std::string &t_pathShaderFile, Primitive *t_primitive, GLuint t_particleAmount, glm::vec2 t_particleSize, glm::vec2 t_particlePosition, glm::vec3 t_particleColor, const std::string &t_pathImageFile = std::string());
        GLvoid renderParticles();
        GLboolean &isParticleBlockes();
};

#endif //SCENE_PARTICLE_GENERATOR_HPP