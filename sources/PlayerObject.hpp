/*
    PlayerObject.hpp
    Purpose: Player object.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef PLAYER_OBJECT_HPP
#define PLAYER_OBJECT_HPP

#include "CommonIncludes.hpp"
#include "Object.hpp"
#include "ObjectParticleGenerator.hpp"
#include "ShotObject.hpp"

class PlayerObject {
    private:
        Object                   m_playerObject;
        GLfloat                  m_playerVelocity;
        GLfloat                  m_playerLife;
        ShotObject               *m_shotObject;
        ObjectParticleGenerator  *m_partcileGenerator;
        std::vector<ShotObject>   m_shots;
        
        GLvoid checkShots();
        GLvoid movePlayer();
    public:
        PlayerObject();
        GLvoid playerLogic();
        GLvoid playerRender();
};

#endif //PLAYER_OBJECT_HPP